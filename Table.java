// compile... javac Table.java
// run... java Table
//
import java.lang.Math;

public class Table{
    public static void main(String args[]){
        boolean []p=new boolean[8];  
        boolean []q=new boolean[8]; 
        boolean []r=new boolean[8]; 
        boolean []pqIMPr=new boolean[8]; 
        boolean []qIMPp=new boolean[8]; 
        boolean []kb=new boolean[8]; 
        boolean []kbIMPr=new boolean[8]; 
        String []header=new String[9]; 
        int i;

        p[0]=true;p[1]=true;p[2]=true;p[3]=true;p[4]=false;p[5]=false;p[6]=false;p[7]=false;
        q[0]=true;q[1]=true;q[2]=false;q[3]=false;q[4]=true;q[5]=true;q[6]=false;q[7]=false;
        r[0]=true;r[1]=false;r[2]=true;r[3]=false;r[4]=true;r[5]=false;r[6]=true;r[7]=false;
        pqIMPr[0]=true;pqIMPr[1]=false;pqIMPr[2]=true;pqIMPr[3]=true;pqIMPr[4]=true;pqIMPr[5]=true;pqIMPr[6]=true;pqIMPr[7]=true;
        qIMPp[0]=true;qIMPp[1]=true;qIMPp[2]=true;qIMPp[3]=true;qIMPp[4]=false;qIMPp[5]=false;qIMPp[6]=true;qIMPp[7]=true;
        kb[0]=true;kb[1]=false;kb[2]=false;kb[3]=false;kb[4]=false;kb[5]=false;kb[6]=false;kb[7]=false;
        kbIMPr[0]=true;kbIMPr[1]=true;kbIMPr[2]=true;kbIMPr[3]=true;kbIMPr[4]=true;kbIMPr[5]=true;kbIMPr[6]=true;kbIMPr[7]=true;
        // kb=(pANDq IMP r)AND(q IMP p)ANDq
        header[0]="p";header[1]="q";header[2]="r";header[3]="(pANDq) => r";header[4]="q => p";header[5]="q";header[6]="kb";header[7]="r";header[8]="kb => r";
        for(i=0;i<9;i++){
            System.out.print(header[i]+"\t\t");
        }

        System.out.println("");

        for(i=0;i<8;i++){
            System.out.print(p[i]+"\t\t"+q[i]+"\t\t"+r[i]+"\t\t"+pqIMPr[i]+"\t\t"+qIMPp[i]+"\t\t"+q[i]+"\t\t"+kb[i]+"\t\t"+r[i]+"\t\t"+kbIMPr[i]);
            System.out.println("");
        }
    }
}
